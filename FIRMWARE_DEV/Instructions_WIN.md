# Installing Firmware Development tools (Macos)

## Part 1: Mingw

1. Download the mingw installer from https://osdn.net/projects/mingw/downloads/68260/mingw-get-setup.exe/
2. choose "mingw32-base-bin" and "mingw32-gcc-g++-bin", on the popup, click "mark for installation"
3. in the menu bar, choose "installation-> apply changes", then click "apply"

## Part 2: Git

1. download and install from https://git-scm.com/download/win
2. on the "Adjusting your PATH environment" page of the installer, choose option "Git from the command line and also from 3rd-party software"
3. on "configuring the line ending conversions" page, choose "checkout as-is, commit unix-style endings"

## Part 3: OpenOCD
1. Download the "xpack" fork of openocd from: https://github.com/xpack-dev-tools/openocd-xpack/releases/
2. extract it to a convenient location that you will remember, and arent likely to delete accidentally (i.e. not in the downloads folder)

## Part 4: USB Drivers
(Everyones favourite topic)

1. Install Stlink usb drivers (for communicating with stm32 devboards): https://www.st.com/en/development-tools/stsw-link009.html
2. Install Jlink USB Drivers (for communicating with solarcar devices): https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack
   Scroll down the page to " J-Link Software and Documentation Pack", click "click for downloads", and click on the download button next to "J-Link Software and Documentation pack for Windows"

## Part 5: Arm Build tools
1. Download and install the arm gcc toolchain: https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads
2. on the final page, make sure you tick "add path to environment variable"

## Part 6: Stm32cubemx

1. install java 8 from https://www.java.com/en/download/ Note that you need Java Runtime Environment (JRE) 8 to run stmcube, but if you are working with the Telemetry server as well, that needs JDK 13
2. in the `FIRMWARE_DEV/resources` folder in this repository, run `SetupSTM32CubeMX.exe`

## part 7: Clion

1. Download the latest version of CLion from <https://www.jetbrains.com/clion/download/download-thanks.html?platform=windows>
2. complete the installation, register an account if necessary,
3. open the settings page (from the startup page, click configure at the bottom then settings, otherwise, File->settings)
4. open "Build, Execution, Deployment" -> "Embedded Development"
5. Click the browse button next to "openocd location", find the folder you saved openocd to in part 3. find "bin\openocd.exe" and click "ok"
6. click "test"
7. If you left the default location when installing stmcubemx in part 5,  
8. on the toolchains page (if you close it, it is accessible from settings->Build, Execution, Deployment -> Toolchains), choose mingw
