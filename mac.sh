#!/bin/sh

installfirmware=1
installjava=1
installweb=1


POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
    --web)
    installweb=1
    shift
    ;;
    --fw)
    installfirmware=1
    shift
    ;;
    --java)
    installjava=1
    shift
    ;;
    --noweb)
    installweb=0
    shift
    ;;
    --nofw)
    installfirmware=0
    shift
    ;;
    --nojava)
    installjava=0
    shift
    ;;
    *)    # unknown option
      POSITIONAL+=("$1") # save it in an array for later
      shift # past argument
      ;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters


echo "Firmware:\t$installfirmware\t(use '--fw' and '--nofw' flags to toggle)"
echo "Java:\t\t$installjava\t(use '--java' and '--nojava' flags to toggle)"
echo "Web:\t\t$installweb\t(use '--web' and '--noweb' flags to toggle)"

taps=()
pkgs=("git")

brewfile=""
commands=()
commandspost=()



if [ ! `command -v brew` ]; then
  commands+=('/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"')
fi

if [[ $installjava -eq 1 ]]; then
  taps+=('adoptopenjdk/openjdk')
  pkgs+=('maven' 'adoptopenjdk13' 'intellij-idea-ce')

fi
if [[ $installweb -eq 1 ]]; then
  pkgs+=('nodejs' 'webstorm')
  commandspost+=("npm install --global nvm@latest")
fi
BASE_DIR="$(cd "$(dirname "$0")"; pwd)";


if [[ $installfirmware -eq 1 ]]; then
  taps+=('ArmMbed/homebrew-formulae')
  pkgs+=('nodejs' 'arm-none-eabi-gcc' 'cmake' 'make' 'gcc' 'clion')
  # taps+=('adoptopenjdk/openjdk')
  # pkgs+=('adoptopenjdk8')

    commandspost+=("npm install --global xpm@latest"
    "mkdir -p ~/.xpack && cd ~/.xpack"
     "xpm init"
     "xpm install @xpack-dev-tools/arm-none-eabi-gcc@latest"
     "xpm install @xpack-dev-tools/openocd@latest"
     "ls -al ~/.xpack/xpacks/.bin"
     "echo 'export PATH=\"\$PATH:~/.xpack/xpacks/.bin\"' >> ~/.zshrc"
     "echo 'export PATH=\"\$PATH:~/.xpack/xpacks/.bin\"' >> ~/.bashrc"
     )

installconfig='<?xml version="1.0" encoding="UTF-8" standalone="no"?><AutomatedInstallation langpack="eng"><com.st.microxplorer.install.MXHTMLHelloPanel id="readme"/><com.st.microxplorer.install.MXLicensePanel id="licence.panel"/><com.st.microxplorer.install.MXAnalyticsPanel id="analytics.panel"/><com.st.microxplorer.install.MXTargetPanel id="target.panel"><installpath>/Applications/STMicroelectronics/STM32CubeMX.app/Contents/Resources</installpath></com.st.microxplorer.install.MXTargetPanel><com.st.microxplorer.install.MXShortcutPanel id="shortcut.panel"/><com.st.microxplorer.install.MXInstallPanel id="install.panel"/><com.st.microxplorer.install.MXFinishPanel id="finish.panel"/></AutomatedInstallation>'
runscript='#!/bin/sh\n\nBASE_DIR="$(cd "$(dirname "$0")"; pwd)";\n
cubemx=$(ls $BASE_DIR/STM32CubeMX.app/Contents/MacOs/STM32CubeMX)
$cubemx $@'
runscriptpath="/Applications/STMicroelectronics/START_STM32CubeMX"
     commandspost+=(""
     'tmpdir="$(mktemp -d)"'
     'cd "$tmpdir"'
     'curl -o "stm32cubemx-mac.zip" "https://www.st.com/content/ccc/resource/technical/software/sw_development_suite/group0/e2/4f/c2/a9/f5/d1/40/5e/stm32cubemx-mac/files/stm32cubemx-mac.zip/jcr:content/translations/en.stm32cubemx-mac.zip"'
     'unzip "stm32cubemx-mac.zip"'
     'installer="$(ls ./SetupSTM32CubeMX-*.app/Contents/MacOs/SetupSTM32CubeMX-*)"'
     "echo '\$installconfig' | \$installer /dev/stdin"
     "echo -e '$runscript' > $runscriptpath"
     "chmod +x $runscriptpath"
     )
fi




for i in "${taps[@]}"; do
  commands+=("brew tap $i")
done
if [[ ${#pkgs[@]} -ne 0 ]]; then
  pkgs=${pkgs[@]}
  commands+=("brew install $pkgs")
fi
commands+=("${commandspost[@]}")

echo "About to run the following commands!"
for i in "${commands[@]}"; do
  echo "  $i"
done

echo "Press Enter to continue, or ESC to cancel!"
while read -r -n1 key; do
  if [[ $key == $'\e' ]];then
    echo "Cancelled!"
    exit 1
  elif [[ $key == $'\0A' ]];then
    echo "Continuing."
    break;
  fi
done

for i in "${commands[@]}"; do
  bash -s <<<"$i"
done
